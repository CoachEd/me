# me

This is my GitLab Page.

Note: wait for pipeline to complete before navigating to a changed page.

Sample URLs:

[https://coached.gitlab.io/me](https://coached.gitlab.io/me)

[https://coached.gitlab.io/me/index.html](https://coached.gitlab.io/me/index.html)

[https://coached.gitlab.io/me/zero/]https://coached.gitlab.io/me/zero/)
